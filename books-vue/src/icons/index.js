/**
 * 字Font icons, uniform use of SVG Sprite vector icons(http://www.iconfont.cn/）
 */
import Vue from 'vue'
import IconSvg from '@/components/icon-svg'
import './iconfont.js'

Vue.component('IconSvg', IconSvg)

const svgFiles = require.context('./svg', true, /\.svg$/)
const iconList = svgFiles.keys().map(item => svgFiles(item))

export default {
  getNameList () {
    return iconList.map(item => item.default.id.replace('icon-', ''))
  }
}
