export default {
  namespaced: true,
  state: {
    // Visible height of the page document (changes size with the window)
    documentClientHeight: 0,
    // navigation bar, layout style, defalut(default) / inverse(reverse)
    navbarLayoutType: 'default',
    // Sidebar, Layout skin, light(light) / dark(black)
    sidebarLayoutSkin: 'dark',
    // Sidebar, Folded state
    sidebarFold: false,
    // Sidebar, Menu
    menuList: [],
    menuActiveName: '',
    // Content, whether to refresh
    contentIsNeedRefresh: false,
    // Main entrance tab
    mainTabs: [],
    mainTabsActiveName: ''
  },
  mutations: {
    updateDocumentClientHeight (state, height) {
      state.documentClientHeight = height
    },
    updateNavbarLayoutType (state, type) {
      state.navbarLayoutType = type
    },
    updateSidebarLayoutSkin (state, skin) {
      state.sidebarLayoutSkin = skin
    },
    updateSidebarFold (state, fold) {
      state.sidebarFold = fold
    },
    updateMenuList (state, list) {
      state.menuList = list
    },
    updateMenuActiveName (state, name) {
      state.menuActiveName = name
    },
    updateContentIsNeedRefresh (state, status) {
      state.contentIsNeedRefresh = status
    },
    updateMainTabs (state, tabs) {
      state.mainTabs = tabs
    },
    updateMainTabsActiveName (state, name) {
      state.mainTabsActiveName = name
    }
  }
}
