/**
 * Production environment
 */
;(function () {
  window.SITE_CONFIG = {};

  window.SITE_CONFIG['baseUrl'] = 'http://localhost:5000/yemingdui';

  window.SITE_CONFIG['domain']  = './';
  window.SITE_CONFIG['version'] = '';
  window.SITE_CONFIG['cdnUrl']  = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
})();
