/**
 * Development Environment
 */
;(function () {
  window.SITE_CONFIG = {};

  // api interface request address
  window.SITE_CONFIG['baseUrl'] = 'http://localhost:5000/server';

  window.SITE_CONFIG['domain']  = './';
  window.SITE_CONFIG['version'] = '';
  window.SITE_CONFIG['cdnUrl']  = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
})();
