package com.yemingdui.validator;

import com.yemingdui.exception.RRException;
import org.apache.commons.lang.StringUtils;

/**
 * Data Validation
 *
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new RRException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new RRException(message);
        }
    }
}
