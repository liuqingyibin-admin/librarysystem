package com.yemingdui.xss;

import com.yemingdui.exception.RRException;
import org.apache.commons.lang.StringUtils;

/**
 * SQLFilter
 *
 */
public class SQLFilter {

    /**
     * SQLFilter
     * @param str  String to be verified
     */
    public static String sqlInject(String str){
        if(StringUtils.isBlank(str)){
            return null;
        }
        //Remove the '|"|;|\ character
        str = StringUtils.replace(str, "'", "");
        str = StringUtils.replace(str, "\"", "");
        str = StringUtils.replace(str, ";", "");
        str = StringUtils.replace(str, "\\", "");

        //Convert to lowercase
        str = str.toLowerCase();

        //Illegal characters
        String[] keywords = {"master", "truncate", "insert", "select", "delete", "update", "declare", "alter", "drop"};

        //Determine if it contains illegal characters
        for(String keyword : keywords){
            if(str.indexOf(keyword) != -1){
                throw new RRException("Contains illegal characters");
            }
        }

        return str;
    }
}
