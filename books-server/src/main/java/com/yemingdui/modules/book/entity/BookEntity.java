package com.yemingdui.modules.book.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: book
 */
@Data
@TableName("book")
public class BookEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;

    private Long typeId;

    @TableField(exist=false)
    private String typeName;

    private String title;

    private String author;

    private String contributor;

    private String publishedBy;

    private String edition;

    private String time;

    private String keyWords;

    private String classification;

    private Integer copyNumber;

    private String citation;

    private Long createUserId;

    private Date createTime;
}
