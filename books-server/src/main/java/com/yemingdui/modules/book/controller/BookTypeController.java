package com.yemingdui.modules.book.controller;

import com.yemingdui.exception.RRException;
import com.yemingdui.modules.book.entity.BookTypeEntity;
import com.yemingdui.modules.book.service.BookTypeService;
import com.yemingdui.modules.sys.controller.AbstractController;
import com.yemingdui.utils.R;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: Book Category Management
 */
@RestController
@RequestMapping("/book/type")
public class BookTypeController extends AbstractController {

    @Autowired
    private BookTypeService bookTypeService;

    /**
     * All Categories List
     */
    @GetMapping("/list")
    @RequiresPermissions("book:type:list")
    public List<BookTypeEntity> list(@RequestParam("name") String name){
        List<BookTypeEntity> typeList = StringUtils.isNotEmpty(name)?bookTypeService.listByName(name):bookTypeService.list();
        for(BookTypeEntity bookTypeEntity : typeList){
            BookTypeEntity parentTypeEntity = bookTypeService.getById(bookTypeEntity.getParentId());
            if(parentTypeEntity != null){
                bookTypeEntity.setParentName(parentTypeEntity.getName());
            }
        }

        return typeList;
    }

    /**
     * Select category (add, modify category)
     */
    @GetMapping("/select")
    @RequiresPermissions("book:type:select")
    public R select(){
        //Query all first-level category list data
        List<BookTypeEntity> typeList = bookTypeService.queryListParentId(0L);

        //Add top menu
        BookTypeEntity root = new BookTypeEntity();
        root.setId(0L);
        root.setName("The primary classification");
        root.setParentId(-1L);
        typeList.add(root);

        return R.ok().put("typeList", typeList);
    }

    /**
     * Classifieds
     */
    @GetMapping("/info/{id}")
    @RequiresPermissions("book:type:info")
    public R info(@PathVariable("id") Long id){
        BookTypeEntity type = bookTypeService.getById(id);
        return R.ok().put("type", type);
    }

    /**
     * save
     */
    @PostMapping("/save")
    @RequiresPermissions("book:type:save")
    public R save(@RequestBody BookTypeEntity type){
        //Data Validation
        verifyForm(type);

        bookTypeService.save(type);

        return R.ok();
    }

    /**
     * update
     */
    @PostMapping("/update")
    @RequiresPermissions("book:type:update")
    public R update(@RequestBody BookTypeEntity type){
        //数据校验
        verifyForm(type);

        bookTypeService.updateById(type);

        return R.ok();
    }

    /**
     * delete
     */
    @PostMapping("/delete/{id}")
    @RequiresPermissions("book:type:delete")
    public R delete(@PathVariable("id") long id){
        //Determine if there are subcategories
        List<BookTypeEntity> typeList = bookTypeService.queryListParentId(id);
        if(typeList.size() > 0){
            return R.error("Please delete subcategories first");
        }

        bookTypeService.removeById(id);

        return R.ok();
    }

    /**
     * Verify that the parameters are correct
     */
    private void verifyForm(BookTypeEntity type){
        if(StringUtils.isBlank(type.getName())){
            throw new RRException("The category name cannot be empty");
        }
    }
}
