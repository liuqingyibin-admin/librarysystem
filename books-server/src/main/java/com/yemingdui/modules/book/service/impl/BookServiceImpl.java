package com.yemingdui.modules.book.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yemingdui.modules.book.entity.BookEntity;
import com.yemingdui.modules.book.mapper.BookMapper;
import com.yemingdui.modules.book.service.BookService;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: Library Management
 */
@Service("bookService")
public class BookServiceImpl extends ServiceImpl<BookMapper, BookEntity> implements BookService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String title = (String)params.get("title");
        String author = (String)params.get("author");
        String keyWords = (String)params.get("keyWords");
        String classification = (String)params.get("classification");
        String typeId = (String)params.get("typeId");
        IPage<BookEntity> page = new Query<BookEntity>().getPage(params);
        return new PageUtils(baseMapper.listPage(page, title,author,keyWords,classification,typeId));
    }

    @Override
    public List<Map<String, Object>> getBorrowTop() {
        return baseMapper.getBorrowTop();
    }

    @Override
    public void addCopyNumber(Long bookId) {
        baseMapper.addCopyNumber(bookId);
    }

    @Override
    public void delCopyNumber(Long id) {
        baseMapper.delCopyNumber(id);
    }

}
