package com.yemingdui.modules.book.controller;

import com.yemingdui.modules.book.service.BookBorrowService;
import com.yemingdui.modules.sys.controller.AbstractController;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description: Book Checkout
 */
@RestController
@RequestMapping("/book/borrow")
public class BookBorrowController extends AbstractController {
    @Autowired
    private BookBorrowService bookBorrowService;

    /**
     * All checkout list
     */
    @GetMapping("/list")
    @RequiresPermissions("book:borrow:list")
    public R list(@RequestParam Map<String, Object> params){
        String isMyBorrow = (String)params.get("isMyBorrow");
        if(isMyBorrow != null){
            params.put("userId", getUserId());
        }
        PageUtils page = bookBorrowService.queryPage(params);
        return R.ok().put("page", page);
    }
    /**
     * All Overdue List
     */
    @GetMapping("/beyond")
    @RequiresPermissions("book:borrow:list")
    public R beyond(){
        Integer count = bookBorrowService.beyond(getUserId());
        return R.ok().put("count", count);
    }

}
