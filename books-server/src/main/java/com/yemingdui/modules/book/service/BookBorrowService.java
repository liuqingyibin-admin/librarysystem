package com.yemingdui.modules.book.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yemingdui.modules.book.entity.BookStockEntity;
import com.yemingdui.utils.PageUtils;

import java.util.Map;

public interface BookBorrowService extends IService<BookStockEntity> {
    PageUtils queryPage(Map<String, Object> params);

    Integer beyond(Long userId);
}
