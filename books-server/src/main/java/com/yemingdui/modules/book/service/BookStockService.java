package com.yemingdui.modules.book.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yemingdui.modules.book.entity.BookEntity;
import com.yemingdui.modules.book.entity.BookStockEntity;
import com.yemingdui.modules.book.entity.BorrowVo;
import com.yemingdui.utils.PageUtils;

import java.util.List;
import java.util.Map;

public interface BookStockService extends IService<BookStockEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void stockReserve(Integer id);

    void stockBorrow(BorrowVo borrowVo);
}
