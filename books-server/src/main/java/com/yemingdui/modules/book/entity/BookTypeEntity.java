package com.yemingdui.modules.book.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: Book Categories
 */
@Data
@TableName("book_type")
public class BookTypeEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId
    private Long id;

    /**
     * Parent category ID, the first category is 0, up to two levels
     */
    private Long parentId;

    /**
     * Parent Category Name
     */
    @TableField(exist=false)
    private String parentName;

    /**
     * 分类名称
     */
    private String name;
}
