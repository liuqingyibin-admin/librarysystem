package com.yemingdui.modules.book.controller;

import com.yemingdui.exception.RRException;
import com.yemingdui.modules.book.entity.BookEntity;
import com.yemingdui.modules.book.entity.BookStockEntity;
import com.yemingdui.modules.book.entity.BorrowVo;
import com.yemingdui.modules.book.service.BookService;
import com.yemingdui.modules.book.service.BookStockService;
import com.yemingdui.modules.sys.controller.AbstractController;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.R;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;

/**
 * @description: Copy Management
 */
@RestController
@RequestMapping("/book/book")
public class BookStockController extends AbstractController {

    @Autowired
    private BookStockService bookStockService;

    @Autowired
    private BookService bookService;

    @GetMapping("/stockList")
    @RequiresPermissions("book:book:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = bookStockService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * stockSave
     */
    @PostMapping("/stockSave")
    @RequiresPermissions("book:book:save")
    public R save(@RequestBody BookStockEntity bookStockEntity){
        //Data Validation
        verifyForm(bookStockEntity);

        bookStockService.save(bookStockEntity);

        bookService.addCopyNumber(bookStockEntity.getBookId());

        return R.ok();
    }

    /**
     * stockUpdate
     */
    @PostMapping("/stockUpdate")
    @RequiresPermissions("book:book:update")
    public R update(@RequestBody BookStockEntity bookStockEntity){
        //Data Validation
        verifyForm(bookStockEntity);

        bookStockService.updateById(bookStockEntity);

        return R.ok();
    }

    /**
     * stockDelete
     */
    @PostMapping("/stockDelete")
    @RequiresPermissions("book:book:delete")
    public R delete(@RequestBody Long[] ids){

        bookStockService.removeByIds(Arrays.asList(ids));

        bookService.delCopyNumber(ids[0]);

        return R.ok();
    }

    @PostMapping("/stockBorrow")
    @RequiresPermissions("book:book:borrow")
    public R stockBorrow(@RequestBody BorrowVo borrowVo){
        borrowVo.setBorrower(getUser().getUsername());
        borrowVo.setCreateUserId(getUserId());
        bookStockService.stockBorrow(borrowVo);

        return R.ok();
    }

    @PostMapping("/stockReserve/{id}")
    @RequiresPermissions("book:book:reserve")
    public R stockReserve(@PathVariable Integer id){

        bookStockService.stockReserve(id);

        return R.ok();
    }

    /**
     * Verify that the parameters are correct
     */
    private void verifyForm(BookStockEntity bookStockEntity){
        if(StringUtils.isBlank(bookStockEntity.getStock())){
            throw new RRException("The Sock cannot be empty");
        }
    }
}
