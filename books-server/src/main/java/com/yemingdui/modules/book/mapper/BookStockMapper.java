package com.yemingdui.modules.book.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yemingdui.modules.book.entity.BookEntity;
import com.yemingdui.modules.book.entity.BookStockEntity;
import com.yemingdui.modules.book.entity.BorrowVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


@Mapper
public interface BookStockMapper extends BaseMapper<BookStockEntity> {
    @Select("SELECT\n" +
            "	*\n" +
            "FROM\n" +
            "	book_stock bs\n" +
            "WHERE\n" +
            "	bs.book_id = #{bookId}\n" +
            "ORDER BY\n" +
            "	bs.id DESC")
    IPage<BookStockEntity> listPage(IPage<BookStockEntity> page, @Param("bookId") String bookId);

    @Update("UPDATE book_stock bs\n" +
            "SET bs.`condition` = 1,\n" +
            " bs.borrower = '', \n" +
            " bs.borrowed_period= null,\n" +
            " bs.create_user_id = null, \n" +
            " bs.create_time = null\n" +
            "WHERE\n" +
            "	bs.id =#{id}")
    void stockReserve(@Param("id") Integer id);

    @Update("UPDATE book_stock bs\n" +
            "SET bs.`condition` = 0,\n" +
            " bs.borrower = #{borrowVo.borrower}, \n" +
            " bs.borrowed_period= #{borrowVo.borrowedPeriod},\n" +
            " bs.create_user_id = #{borrowVo.createUserId}, \n" +
            " bs.create_time = NOW()\n" +
            "WHERE\n" +
            "	bs.id =#{borrowVo.id}")
    void stockBorrow(@Param("borrowVo") BorrowVo borrowVo);
}
