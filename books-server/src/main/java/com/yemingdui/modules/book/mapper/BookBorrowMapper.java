package com.yemingdui.modules.book.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yemingdui.modules.book.entity.BookStockEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface BookBorrowMapper extends BaseMapper<BookStockEntity> {

    @Select({"<script>",
            "SELECT\n" +
            "	b.title,\n" +
            "	bs.*\n" +
            "FROM\n" +
            "	book_stock bs,\n" +
            "	book b\n" +
            "WHERE\n" +
            "	bs.book_id = b.id" +
            "<when test='title!=null'>",
            "AND b.title like CONCAT('%',#{title},'%')",
            "</when>",
            "<when test='stock!=null'>",
            "AND bs.stock like CONCAT('%',#{stock},'%')",
            "</when>",
            "<when test='userId!=null'>",
            "AND bs.create_user_id = #{userId}",
            "</when>",
            "order by bs.id DESC" ,
            "</script>"})
    IPage<BookStockEntity> listPage(IPage<BookStockEntity> page, @Param("title") String title, @Param("stock") String stock, @Param("userId") Long userId);

    @Select("SELECT\n" +
            "	count(1) AS count\n" +
            "FROM\n" +
            "	book_stock bs\n" +
            "WHERE\n" +
            "	bs.create_user_id = #{userId}\n" +
            "AND `condition` = 0\n" +
            "AND datediff(NOW(), bs.create_time) > bs.borrowed_period")
    Integer beyond(@Param("userId") Long userId);

    @Select({"<script>",
            "SELECT\n" +
            "	b.title,\n" +
            "	bs.*\n" +
            "FROM\n" +
            "	book_stock bs,\n" +
            "	book b\n" +
            "WHERE\n" +
            "	bs.book_id = b.id\n" +
            "AND bs.`condition` = 0\n" +
            "AND datediff(NOW(), bs.create_time) > bs.borrowed_period\n" +
            "<when test='title!=null'>",
            "AND b.title like CONCAT('%',#{title},'%')",
            "</when>",
            "<when test='stock!=null'>",
            "AND bs.stock like CONCAT('%',#{stock},'%')",
            "</when>",
            "order by bs.id DESC" ,
            "</script>"})
    IPage<BookStockEntity> listOrverPage(IPage<BookStockEntity> page, @Param("title") String title, @Param("stock")  String stock);
}
