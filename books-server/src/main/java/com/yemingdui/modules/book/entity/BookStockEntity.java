package com.yemingdui.modules.book.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 */
@Data
@TableName("book_stock")
public class BookStockEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;

    private Long bookId;

    @TableField(exist=false)
    private String title;

    private String stock;

    private String place;

    private Integer condition;

    private String borrower;

    private Integer borrowedPeriod;

    private Long createUserId;

    private Date createTime;
}
