package com.yemingdui.modules.book.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 */
@Data
public class BorrowVo implements Serializable {
    private Integer id;
    private Integer borrowedPeriod;
    private String borrower;
    private Long createUserId;
}
