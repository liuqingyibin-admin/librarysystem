package com.yemingdui.modules.book.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yemingdui.modules.book.entity.BookTypeEntity;
import com.yemingdui.modules.book.mapper.BookTypeMapper;
import com.yemingdui.modules.book.service.BookTypeService;
import com.yemingdui.utils.MapUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: Book Categories
 */
@Service("bookTypeService")
public class BookTypeServiceImpl extends ServiceImpl<BookTypeMapper, BookTypeEntity> implements BookTypeService {

    @Override
    public List<BookTypeEntity> queryListParentId(long parentId) {
        return baseMapper.queryListParentId(parentId);
    }

    @Override
    public List<BookTypeEntity> listByName(String name) {
        return baseMapper.listByName(name);
    }

}
