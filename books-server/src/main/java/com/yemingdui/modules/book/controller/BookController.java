package com.yemingdui.modules.book.controller;

import com.yemingdui.exception.RRException;
import com.yemingdui.modules.book.entity.BookEntity;
import com.yemingdui.modules.book.service.BookService;
import com.yemingdui.modules.sys.controller.AbstractController;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.R;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description: Library Management
 */
@RestController
@RequestMapping("/book/book")
public class BookController extends AbstractController {

    @Autowired
    private BookService bookService;

    /**
     * All Books List
     */
    @GetMapping("/list")
    @RequiresPermissions("book:book:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = bookService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * Book Information
     */
    @GetMapping("/info/{id}")
    @RequiresPermissions("book:book:info")
    public R info(@PathVariable("id") Long id){
        BookEntity book = bookService.getById(id);
        return R.ok().put("book", book);
    }

    /**
     * save
     */
    @PostMapping("/save")
    @RequiresPermissions("book:book:save")
    public R save(@RequestBody BookEntity book){
        //数据校验
        verifyForm(book);

        bookService.save(book);

        return R.ok();
    }

    /**
     * update
     */
    @PostMapping("/update")
    @RequiresPermissions("book:book:update")
    public R update(@RequestBody BookEntity book){
        //Data Validation
        verifyForm(book);

        bookService.updateById(book);

        return R.ok();
    }

    /**
     * delete
     */
    @PostMapping("/delete")
    @RequiresPermissions("book:book:delete")
    public R delete(@RequestBody Long[] roleIds){

        bookService.removeByIds(Arrays.asList(roleIds));

        return R.ok();
    }


    /**
     * Borrowing ranking top10
     */
    @GetMapping("/getBorrowTop")
    public R getBorrowTop(){
        return R.ok().put("data", bookService.getBorrowTop());
    }

    /**
     * Verify that the parameters are correct
     */
    private void verifyForm(BookEntity book){
        if(StringUtils.isBlank(book.getTitle())){
            throw new RRException("The Title cannot be empty");
        }
    }
}
