package com.yemingdui.modules.book.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yemingdui.modules.book.entity.BookEntity;
import com.yemingdui.utils.PageUtils;

import java.util.List;
import java.util.Map;

public interface BookService extends IService<BookEntity> {
    PageUtils queryPage(Map<String, Object> params);

    List<Map<String, Object>>  getBorrowTop();

    void addCopyNumber(Long bookId);

    void delCopyNumber(Long id);
}
