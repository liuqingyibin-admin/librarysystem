package com.yemingdui.modules.book.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yemingdui.modules.book.entity.BookTypeEntity;

import java.util.List;

public interface BookTypeService extends IService<BookTypeEntity> {

    List<BookTypeEntity> queryListParentId(long parentId);

    List<BookTypeEntity> listByName(String name);
}
