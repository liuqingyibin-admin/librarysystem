package com.yemingdui.modules.book.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yemingdui.modules.book.entity.BookEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

@Mapper
public interface BookMapper extends BaseMapper<BookEntity> {
    @Select({"<script>",
            "SELECT" ,
            "	b.id," ,
            "	b.type_id," ,
            "	bt.name AS typeName," ,
            "   b.title," ,
            "	b.author," ,
            "	b.contributor," ,
            "	b.published_by," ,
            "	b.edition," ,
            "	b.time," ,
            "	b.key_words," ,
            "	b.classification," ,
            "	b.copy_number," ,
            "	b.citation," ,
            "	b.create_user_id,",
            "	b.create_time" ,
            "FROM" ,
            "	book b" ,
            "LEFT JOIN book_type bt ON b.type_id = bt.id" ,
            "WHERE" ,
            "	1 = 1",
            "<when test='title!=null'>",
            "AND b.title like CONCAT('%',#{title},'%')",
            "</when>",
            "<when test='author!=null'>",
            "AND b.author like CONCAT('%',#{author},'%')",
            "</when>",
            "<when test='keyWords!=null'>",
            "AND b.key_words like CONCAT('%',#{keyWords},'%')",
            "</when>",
            "<when test='classification!=null'>",
            "AND b.classification like CONCAT('%',#{classification},'%')",
            "</when>",
            "<when test='typeId!=null'>",
            "AND (bt.id = #{typeId} OR bt.parent_id = #{typeId})",
            "</when>",
            "ORDER BY b.id DESC",
            "</script>"})
    IPage<BookEntity> listPage(IPage<BookEntity> page, @Param("title") String title, @Param("author") String author, @Param("keyWords") String keyWords, @Param("classification") String classification, @Param("typeId") String typeId);

    @Select("SELECT\n" +
            "	bs.borrower,\n" +
            "	count(1) AS count\n" +
            "FROM\n" +
            "	book_stock bs\n" +
            "WHERE\n" +
            "	bs.`condition` = 0\n" +
            "GROUP BY\n" +
            "	bs.borrower\n" +
            "ORDER BY\n" +
            "	count(1) DESC\n" +
            "LIMIT 10")
    List<Map<String, Object>> getBorrowTop();

    @Update("UPDATE book b\n" +
            "SET b.copy_number = b.copy_number + 1\n" +
            "WHERE\n" +
            "	b.id =#{bookId}")
    void addCopyNumber(@Param("bookId") Long bookId);

    @Update("UPDATE book b\n" +
            "SET b.copy_number = b.copy_number + 1\n" +
            "WHERE\n" +
            "	b.id = (\n" +
            "		SELECT\n" +
            "			bs.book_id\n" +
            "		FROM\n" +
            "			book_stock bs\n" +
            "		WHERE\n" +
            "			bs.id = #{id}\n" +
            "		LIMIT 1\n" +
            "	)")
    void delCopyNumber(@Param("id") Long id);
}
