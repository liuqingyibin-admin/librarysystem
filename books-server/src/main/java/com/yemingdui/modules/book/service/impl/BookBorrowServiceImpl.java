package com.yemingdui.modules.book.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yemingdui.modules.book.entity.BookStockEntity;
import com.yemingdui.modules.book.mapper.BookBorrowMapper;
import com.yemingdui.modules.book.service.BookBorrowService;
import com.yemingdui.modules.book.service.BookService;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @description: Book Checkout
 */
@Service("bookBorrowService")
public class BookBorrowServiceImpl extends ServiceImpl<BookBorrowMapper, BookStockEntity> implements BookBorrowService {

    @Autowired
    private BookService bookService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Long userId = (Long)params.get("userId");
        String title = (String)params.get("title");
        String stock = (String)params.get("stock");
        IPage<BookStockEntity> page = new Query<BookStockEntity>().getPage(params);
        if(userId != null){
            return new PageUtils(baseMapper.listPage(page, title, stock, userId));
        }else {
            return new PageUtils(baseMapper.listOrverPage(page, title, stock));
        }

    }

    @Override
    public Integer beyond(Long userId) {
        return baseMapper.beyond(userId);
    }

}
