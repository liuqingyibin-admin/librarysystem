package com.yemingdui.modules.book.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yemingdui.modules.book.entity.BookTypeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookTypeMapper extends BaseMapper<BookTypeEntity> {

    @Select("SELECT\n" +
            "	*\n" +
            "FROM\n" +
            "	book_type bt\n" +
            "WHERE\n" +
            "	bt.parent_id = #{parentId}")
    List<BookTypeEntity> queryListParentId(@Param("parentId") long parentId);

    @Select("SELECT\n" +
            "	*\n" +
            "FROM\n" +
            "	book_type bt\n" +
            "WHERE\n" +
            "	bt.name like CONCAT('%',#{name},'%')")
    List<BookTypeEntity> listByName(@Param("name") String name);
}
