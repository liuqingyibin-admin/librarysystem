package com.yemingdui.modules.book.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yemingdui.modules.book.entity.BookEntity;
import com.yemingdui.modules.book.entity.BookStockEntity;
import com.yemingdui.modules.book.entity.BorrowVo;
import com.yemingdui.modules.book.mapper.BookMapper;
import com.yemingdui.modules.book.mapper.BookStockMapper;
import com.yemingdui.modules.book.service.BookService;
import com.yemingdui.modules.book.service.BookStockService;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: Copy Management
 */
@Service("bookStockService")
public class BookStockServiceImpl extends ServiceImpl<BookStockMapper, BookStockEntity> implements BookStockService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String bookId = (String)params.get("bookId");
        IPage<BookStockEntity> page = new Query<BookStockEntity>().getPage(params);
        return new PageUtils(baseMapper.listPage(page, bookId));
    }

    @Override
    public void stockReserve(Integer id) {
        baseMapper.stockReserve(id);
    }

    @Override
    public void stockBorrow(BorrowVo borrowVo) {
        baseMapper.stockBorrow(borrowVo);
    }
}
