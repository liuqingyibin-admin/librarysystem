package com.yemingdui.modules.sys.form;

import lombok.Data;

/**
 * Password form
 *
 */
@Data
public class PasswordForm {
    private String password;
    private String newPassword;

}
