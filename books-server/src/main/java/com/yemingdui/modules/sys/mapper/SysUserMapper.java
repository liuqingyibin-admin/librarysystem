package com.yemingdui.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yemingdui.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @description: System Users
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

    List<String> queryAllPerms(Long userId);

    List<Long> queryAllMenuId(Long userId);

    SysUserEntity queryByUserName(String username);
}
