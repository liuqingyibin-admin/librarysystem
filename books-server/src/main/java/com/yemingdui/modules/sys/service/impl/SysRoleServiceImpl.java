package com.yemingdui.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yemingdui.modules.sys.entity.SysRoleEntity;
import com.yemingdui.exception.RRException;
import com.yemingdui.modules.sys.mapper.SysRoleMapper;
import com.yemingdui.modules.sys.service.SysRoleService;
import com.yemingdui.modules.sys.service.SysUserRoleService;
import com.yemingdui.modules.sys.service.SysUserService;
import com.yemingdui.modules.sys.service.SysRoleMenuService;
import com.yemingdui.utils.Constant;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.Query;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Roles
 *
 */
@Service("sysRoleService")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleEntity> implements SysRoleService {
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	@Autowired
	private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String roleName = (String)params.get("roleName");
		Long createUserId = (Long)params.get("createUserId");

		IPage<SysRoleEntity> page = this.page(
			new Query<SysRoleEntity>().getPage(params),
			new QueryWrapper<SysRoleEntity>()
				.like(StringUtils.isNotBlank(roleName),"role_name", roleName)
				.eq(createUserId != null,"create_user_id", createUserId)
		);

		return new PageUtils(page);
	}

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveRole(SysRoleEntity role) {
        role.setCreateTime(new Date());
        this.save(role);

        //Check whether the authority is exceeded
        checkPrems(role);

        //Save role and menu relationships
        sysRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SysRoleEntity role) {
        this.updateById(role);

        //Check whether the authority is exceeded
        checkPrems(role);

        //Update role and menu relationships
        sysRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatch(Long[] roleIds) {
        //Delete Role
        this.removeByIds(Arrays.asList(roleIds));

        //Remove role and menu association
        sysRoleMenuService.deleteBatch(roleIds);

        //Remove role and user associations
        sysUserRoleService.deleteBatch(roleIds);
    }


    @Override
	public List<Long> queryRoleIdList(Long createUserId) {
		return baseMapper.queryRoleIdList(createUserId);
	}

	/**
	 * Check whether the authority is exceeded
	 */
	private void checkPrems(SysRoleEntity role){
		//If you are not a super administrator, you need to determine whether the privileges of the role exceed your own privileges
		if(role.getCreateUserId() == Constant.SUPER_ADMIN){
			return ;
		}

		//Query the list of menus owned by the user
		List<Long> menuIdList = sysUserService.queryAllMenuId(role.getCreateUserId());

		//Determining if you are overstepping your authority
		if(!menuIdList.containsAll(role.getMenuIdList())){
			throw new RRException("The permission of the new role has exceeded your permission scope");
		}
	}
}
