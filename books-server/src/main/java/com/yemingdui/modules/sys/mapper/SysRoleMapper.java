package com.yemingdui.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yemingdui.modules.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Role Management
 *
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

	List<Long> queryRoleIdList(Long createUserId);
}
