package com.yemingdui.modules.sys.service;

import com.yemingdui.modules.sys.entity.SysUserEntity;
import com.yemingdui.modules.sys.entity.SysUserTokenEntity;

import java.util.Set;

/**
 * shiro
 *
 */
public interface ShiroService {
    /**
     * Get a list of user permissions
     */
    Set<String> getUserPermissions(long userId);

    SysUserTokenEntity queryByToken(String token);

    /**
     * According to the user ID, query the user
     * @param userId
     */
    SysUserEntity queryUser(Long userId);
}
