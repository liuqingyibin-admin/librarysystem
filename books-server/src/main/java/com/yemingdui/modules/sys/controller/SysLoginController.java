package com.yemingdui.modules.sys.controller;

import com.yemingdui.modules.sys.entity.SysUserEntity;
import com.yemingdui.modules.sys.form.SysLoginForm;
import com.yemingdui.modules.sys.service.SysCaptchaService;
import com.yemingdui.modules.sys.service.SysUserService;
import com.yemingdui.modules.sys.service.SysUserTokenService;
import com.yemingdui.utils.R;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * Login Related
 *
 */
@RestController
public class SysLoginController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserTokenService sysUserTokenService;
	@Autowired
	private SysCaptchaService sysCaptchaService;

	/**
	 * Verification Code
	 */
	@GetMapping("captcha.jpg")
	public void captcha(HttpServletResponse response, String uuid)throws IOException {
		response.setHeader("Cache-Control", "no-store, no-cache");
		response.setContentType("image/jpeg");

		//Get image verification code
		BufferedImage image = sysCaptchaService.getCaptcha(uuid);

		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(image, "jpg", out);
		IOUtils.closeQuietly(out);
	}

	/**
	 * login
	 */
	@PostMapping("/sys/login")
	public Map<String, Object> login(@RequestBody SysLoginForm form)throws IOException {
		boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
		if(!captcha){
			return R.error("The verification code is incorrect");
		}

		//User Information
		SysUserEntity user = sysUserService.queryByUserName(form.getUsername());

		//Account does not exist, wrong password
		if(user == null || !user.getPassword().equals(new Sha256Hash(form.getPassword(), user.getSalt()).toHex())) {
			return R.error("The account or password is incorrect");
		}

		//Account Lock
		if(user.getStatus() == 0){
			return R.error("The account has been locked. Please contact the administrator");
		}

		//Generate the token and save it to the database
		R r = sysUserTokenService.createToken(user.getUserId());
		return r;
	}


	/**
	 * logout
	 */
	@PostMapping("/sys/logout")
	public R logout() {
		sysUserTokenService.logout(getUserId());
		return R.ok();
	}

}
