package com.yemingdui.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yemingdui.modules.sys.mapper.SysUserTokenMapper;
import com.yemingdui.modules.sys.entity.SysUserTokenEntity;
import com.yemingdui.oauth2.TokenGenerator;
import com.yemingdui.modules.sys.service.SysUserTokenService;
import com.yemingdui.utils.R;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service("sysUserTokenService")
public class SysUserTokenServiceImpl extends ServiceImpl<SysUserTokenMapper, SysUserTokenEntity> implements SysUserTokenService {
	//Expires in 12 hours
	private final static int EXPIRE = 3600 * 12;


	@Override
	public R createToken(long userId) {
		//Generate a token
		String token = TokenGenerator.generateValue();

		Date now = new Date();
		Date expireTime = new Date(now.getTime() + EXPIRE * 1000);

		//Determine if a token has been generated
		SysUserTokenEntity tokenEntity = this.getById(userId);
		if(tokenEntity == null){
			tokenEntity = new SysUserTokenEntity();
			tokenEntity.setUserId(userId);
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			this.save(tokenEntity);
		}else{
			tokenEntity.setToken(token);
			tokenEntity.setUpdateTime(now);
			tokenEntity.setExpireTime(expireTime);

			this.updateById(tokenEntity);
		}

		R r = R.ok().put("token", token).put("expire", EXPIRE);

		return r;
	}

	@Override
	public void logout(long userId) {
		String token = TokenGenerator.generateValue();

		SysUserTokenEntity tokenEntity = new SysUserTokenEntity();
		tokenEntity.setUserId(userId);
		tokenEntity.setToken(token);
		this.updateById(tokenEntity);
	}
}
