package com.yemingdui.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yemingdui.modules.sys.entity.SysRoleMenuEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Role and menu correspondence
 *
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuEntity> {

	List<Long> queryMenuIdList(Long roleId);

	int deleteBatch(Long[] roleIds);
}
