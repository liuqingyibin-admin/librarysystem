package com.yemingdui.modules.sys.service.impl;

import com.yemingdui.modules.sys.mapper.SysMenuMapper;
import com.yemingdui.modules.sys.mapper.SysUserMapper;
import com.yemingdui.modules.sys.mapper.SysUserTokenMapper;
import com.yemingdui.modules.sys.entity.SysMenuEntity;
import com.yemingdui.modules.sys.entity.SysUserEntity;
import com.yemingdui.modules.sys.entity.SysUserTokenEntity;
import com.yemingdui.modules.sys.service.ShiroService;
import com.yemingdui.utils.Constant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShiroServiceImpl implements ShiroService {
    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserTokenMapper sysUserTokenMapper;

    @Override
    public Set<String> getUserPermissions(long userId) {
        List<String> permsList;

        //System administrator with highest authority
        if(userId == Constant.SUPER_ADMIN){
            List<SysMenuEntity> menuList = sysMenuMapper.selectList(null);
            permsList = new ArrayList<>(menuList.size());
            for(SysMenuEntity menu : menuList){
                permsList.add(menu.getPerms());
            }
        }else{
            permsList = sysUserMapper.queryAllPerms(userId);
        }
        //List of user permissions
        Set<String> permsSet = new HashSet<>();
        for(String perms : permsList){
            if(StringUtils.isBlank(perms)){
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }

    @Override
    public SysUserTokenEntity queryByToken(String token) {
        return sysUserTokenMapper.queryByToken(token);
    }

    @Override
    public SysUserEntity queryUser(Long userId) {
        return sysUserMapper.selectById(userId);
    }
}
