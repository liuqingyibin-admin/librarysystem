package com.yemingdui.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yemingdui.modules.sys.entity.SysUserRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * User-role correspondence
 *
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity> {

	/**
	 * Get a list of role IDs based on user IDs
	 */
	List<Long> queryRoleIdList(Long userId);


	/**
	 * Bulk delete based on an array of role IDs
	 */
	int deleteBatch(Long[] roleIds);
}
