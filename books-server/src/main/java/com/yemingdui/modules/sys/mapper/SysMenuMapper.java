package com.yemingdui.modules.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yemingdui.modules.sys.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Menu Management
 *
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {

	List<SysMenuEntity> queryListParentId(Long parentId);

	List<SysMenuEntity> queryNotButtonList();

}
