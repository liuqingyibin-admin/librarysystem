package com.yemingdui.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yemingdui.modules.sys.entity.SysCaptchaEntity;

import java.awt.image.BufferedImage;

/**
 * Verification Code
 *
 */
public interface SysCaptchaService extends IService<SysCaptchaEntity> {

    /**
     * Get image verification code
     */
    BufferedImage getCaptcha(String uuid);

    boolean validate(String uuid, String code);
}
