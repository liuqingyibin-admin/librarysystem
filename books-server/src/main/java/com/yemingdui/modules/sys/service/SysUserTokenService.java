package com.yemingdui.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yemingdui.modules.sys.entity.SysUserTokenEntity;
import com.yemingdui.utils.R;

public interface SysUserTokenService extends IService<SysUserTokenEntity> {

	R createToken(long userId);

	void logout(long userId);

}
