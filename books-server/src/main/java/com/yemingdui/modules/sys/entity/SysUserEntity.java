package com.yemingdui.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yemingdui.validator.group.AddGroup;
import com.yemingdui.validator.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * User
 *
 */
@Data
@TableName("sys_user")
public class SysUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private Long userId;

	@NotBlank(message="Username cannot be empty", groups = {AddGroup.class, UpdateGroup.class})
	private String username;

	@NotBlank(message="Password cannot be empty", groups = AddGroup.class)
	private String password;

	private String salt;

	@NotBlank(message="Mailbox cannot be empty", groups = {AddGroup.class, UpdateGroup.class})
	@Email(message="Incorrect email format", groups = {AddGroup.class, UpdateGroup.class})
	private String email;

	private String mobile;

	private Integer status;

	@TableField(exist=false)
	private List<Long> roleIdList;

	private Long createUserId;

	private Date createTime;

}
