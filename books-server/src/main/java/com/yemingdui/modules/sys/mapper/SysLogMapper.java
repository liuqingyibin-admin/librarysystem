package com.yemingdui.modules.sys.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yemingdui.modules.sys.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * System log
 *
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLogEntity> {

}
