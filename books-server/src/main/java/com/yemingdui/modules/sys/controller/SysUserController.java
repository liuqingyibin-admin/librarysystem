package com.yemingdui.modules.sys.controller;

import com.yemingdui.modules.sys.entity.SysUserEntity;
import com.yemingdui.modules.sys.form.PasswordForm;
import com.yemingdui.modules.sys.service.SysUserRoleService;
import com.yemingdui.modules.sys.service.SysUserService;
import com.yemingdui.utils.Constant;
import com.yemingdui.utils.PageUtils;
import com.yemingdui.utils.R;
import com.yemingdui.validator.Assert;
import com.yemingdui.validator.ValidatorUtils;
import com.yemingdui.validator.group.AddGroup;
import com.yemingdui.validator.group.UpdateGroup;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @description: User Management
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController{
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;


    /**
     * All Users List
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:user:list")
    public R list(@RequestParam Map<String, Object> params){
        //Only super administrators can view the list of all administrators
        if(getUserId() != Constant.SUPER_ADMIN){
            params.put("createUserId", getUserId());
        }
        PageUtils page = sysUserService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * Obtain logged-in user information
     */
    @GetMapping("/info")
    public R info(){
        return R.ok().put("user", getUser());
    }

    /**
     * Change login user password
     */
    @PostMapping("/password")
    public R password(@RequestBody PasswordForm form){
        Assert.isBlank(form.getNewPassword(), "The new password cannot be empty");

        //sha256 Encryption
        String password = new Sha256Hash(form.getPassword(), getUser().getSalt()).toHex();
        //sha256 Encryption
        String newPassword = new Sha256Hash(form.getNewPassword(), getUser().getSalt()).toHex();

        //Update password
        boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
        if(!flag){
            return R.error("The old password is incorrect");
        }

        return R.ok();
    }

    /**
     * User Information
     */
    @GetMapping("/info/{userId}")
    @RequiresPermissions("sys:user:info")
    public R info(@PathVariable("userId") Long userId){
        SysUserEntity user = sysUserService.getById(userId);

        //Get the list of roles to which the user belongs
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
        user.setRoleIdList(roleIdList);

        return R.ok().put("user", user);
    }

    /**
     * save
     */
    @PostMapping("/save")
    @RequiresPermissions("sys:user:save")
    public R save(@RequestBody SysUserEntity user){
        ValidatorUtils.validateEntity(user, AddGroup.class);

        user.setCreateUserId(getUserId());
        sysUserService.saveUser(user);

        return R.ok();
    }

    /**
     * update
     */
    @PostMapping("/update")
    @RequiresPermissions("sys:user:update")
    public R update(@RequestBody SysUserEntity user){
        ValidatorUtils.validateEntity(user, UpdateGroup.class);

        user.setCreateUserId(getUserId());
        sysUserService.update(user);

        return R.ok();
    }

    /**
     * delete
     */
    @PostMapping("/delete")
    @RequiresPermissions("sys:user:delete")
    public R delete(@RequestBody Long[] userIds){
        if(ArrayUtils.contains(userIds, 1L)){
            return R.error("The system administrator cannot delete it");
        }

        if(ArrayUtils.contains(userIds, getUserId())){
            return R.error("The current user cannot be deleted");
        }

        sysUserService.deleteBatch(userIds);

        return R.ok();
    }
}
