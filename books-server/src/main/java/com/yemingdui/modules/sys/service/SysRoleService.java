package com.yemingdui.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yemingdui.modules.sys.entity.SysRoleEntity;
import com.yemingdui.utils.PageUtils;

import java.util.List;
import java.util.Map;


public interface SysRoleService extends IService<SysRoleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	void saveRole(SysRoleEntity role);

	void update(SysRoleEntity role);

	void deleteBatch(Long[] roleIds);

	List<Long> queryRoleIdList(Long createUserId);
}
