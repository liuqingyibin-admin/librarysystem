package com.yemingdui.modules.sys.form;

import lombok.Data;

/**
 * Login Form
 *
 */
@Data
public class SysLoginForm {
    private String username;
    private String password;
    private String captcha;
    private String uuid;


}
