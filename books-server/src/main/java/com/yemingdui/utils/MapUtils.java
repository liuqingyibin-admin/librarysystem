package com.yemingdui.utils;

import java.util.HashMap;


/**
 * Map Tools Class
 *
 */
public class MapUtils extends HashMap<String, Object> {

    @Override
    public MapUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
