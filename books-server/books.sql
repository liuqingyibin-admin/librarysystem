/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50734
Source Host           : localhost:3306
Source Database       : books

Target Server Type    : MYSQL
Target Server Version : 50734
File Encoding         : 65001

Date: 2021-11-17 19:22:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `contributor` varchar(50) DEFAULT NULL,
  `published_by` varchar(50) DEFAULT '',
  `edition` varchar(50) DEFAULT '',
  `time` varchar(50) DEFAULT NULL,
  `key_words` varchar(100) DEFAULT NULL,
  `classification` varchar(100) DEFAULT NULL,
  `copy_number` int(10) DEFAULT '0',
  `citation` varchar(100) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('62', '64', 'Database', 'Yue Li', null, 'ABC Science', '8th', '1980', 'database,textbook', 'Science-CS-Database', '2', 'Algorithm', null, null);
INSERT INTO `book` VALUES ('63', '64', 'Computer Structure', 'Wu', null, 'AK Science', '8th', '1981', 'database , structure', ' Science-Computer-Structure', '1', 'NA', null, null);
INSERT INTO `book` VALUES ('64', '66', 'Algorithm', 'Jack', null, 'Sun Academy', '', '1 May , 1982', 'cs , coding', 'Science-CS-Algorithm', '1', 'NA', null, null);
INSERT INTO `book` VALUES ('65', '65', 'MYSQL', 'Donald', 'Yue', 'South China', '', '1 May , 1983', 'sql', 'Science-CS-MYSQL', '1', 'NA', null, null);
INSERT INTO `book` VALUES ('66', '67', 'How  IIT  Become the Top U in the USA ', 'Tom', null, '', '', '1 May , 2020', 'IIT , school', ' SCI-School-IIT', '1', 'NA', null, null);
INSERT INTO `book` VALUES ('67', '68', 'Why 425 is the Top Course in IIT', 'Tommy', null, '', '', '2 May , 2021', ' IIT 425', 'IIT-CS-425', '1', 'NA', null, null);

-- ----------------------------
-- Table structure for book_stock
-- ----------------------------
DROP TABLE IF EXISTS `book_stock`;
CREATE TABLE `book_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_id` bigint(20) DEFAULT NULL,
  `stock` varchar(50) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `condition` int(2) DEFAULT '1' COMMENT '0：out on loan；1：available',
  `borrower` varchar(50) DEFAULT '',
  `borrowed_period` varchar(50) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of book_stock
-- ----------------------------
INSERT INTO `book_stock` VALUES ('73', '62', 'Copy 1', 'Room 301', '0', 'Student1', '3', '2', '2021-11-11 15:45:08');
INSERT INTO `book_stock` VALUES ('74', '62', 'Copy 2', 'Room 302', '0', 'Student1', '1', '2', '2021-11-11 15:45:03');
INSERT INTO `book_stock` VALUES ('75', '63', ' Copy 1 ', ' Room 302 ', '0', 'Student2', '2', '4', '2021-11-11 15:45:48');
INSERT INTO `book_stock` VALUES ('76', '64', ' Copy 1 ', ' Room 501 ', '1', '', null, null, null);
INSERT INTO `book_stock` VALUES ('77', '65', ' Copy 1 ', ' Room 502 ', '1', '', null, null, null);
INSERT INTO `book_stock` VALUES ('78', '66', ' Copy 1 ', ' Room 601 ', '1', '', null, null, null);
INSERT INTO `book_stock` VALUES ('79', '67', ' Copy 1 ', ' Room 701 ', '1', '', null, null, null);

-- ----------------------------
-- Table structure for book_type
-- ----------------------------
DROP TABLE IF EXISTS `book_type`;
CREATE TABLE `book_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of book_type
-- ----------------------------
INSERT INTO `book_type` VALUES ('64', '0', 'Books');
INSERT INTO `book_type` VALUES ('65', '0', 'Magazine');
INSERT INTO `book_type` VALUES ('66', '0', 'Journal Articles');
INSERT INTO `book_type` VALUES ('67', '0', 'Thesis');
INSERT INTO `book_type` VALUES ('68', '0', 'Technical Reports');

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL,
  `expire_time` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------
INSERT INTO `sys_captcha` VALUES ('03a1360b-89d6-4756-8795-3bebe3107330', '4c8an', '2021-06-18 15:06:09');
INSERT INTO `sys_captcha` VALUES ('051d3e4f-faa4-4d50-89d6-622765d85a00', 'm4ngx', '2021-11-11 20:48:08');
INSERT INTO `sys_captcha` VALUES ('08914ad1-c6f0-4cca-8bfc-a86c0a07e203', '4gep6', '2021-06-18 17:17:39');
INSERT INTO `sys_captcha` VALUES ('08c243ff-508f-4805-8039-501138f636e5', 'n7ap5', '2021-11-11 21:11:31');
INSERT INTO `sys_captcha` VALUES ('0ab495fe-e4df-4820-8b4f-c379737e1ad6', 'm8ga4', '2021-11-11 21:11:41');
INSERT INTO `sys_captcha` VALUES ('103bdeca-d6bd-4402-872b-bbfb1f5be286', 'w72d3', '2021-11-11 21:13:41');
INSERT INTO `sys_captcha` VALUES ('116538ca-4885-4b99-8ff0-cb48a9606b87', '2bfad', '2021-11-09 19:17:53');
INSERT INTO `sys_captcha` VALUES ('1413dca7-b79f-4465-864c-19f18f5a9c3f', '4n2y7', '2021-11-11 21:08:03');
INSERT INTO `sys_captcha` VALUES ('1e058525-a7c8-457c-8db9-ece91ee44e37', 'bn7x3', '2021-11-11 21:09:49');
INSERT INTO `sys_captcha` VALUES ('1f1af227-30d9-4cc9-8133-462c5d1cd7df', '3ay87', '2021-06-18 15:01:08');
INSERT INTO `sys_captcha` VALUES ('1f71fee4-52c9-4b6f-8a48-94d100948608', '5ym8a', '2021-11-11 21:12:42');
INSERT INTO `sys_captcha` VALUES ('21c4ac55-10f7-40b3-84a9-a7120068d03f', 'dpdnw', '2021-11-11 09:11:55');
INSERT INTO `sys_captcha` VALUES ('26e4e0c0-fde0-4566-8c59-08a34c53cc7e', '2pcyw', '2021-11-11 21:09:12');
INSERT INTO `sys_captcha` VALUES ('27814033-9ac4-4218-8de8-98e255f26933', '44g57', '2021-06-18 14:49:17');
INSERT INTO `sys_captcha` VALUES ('29cb2769-ce06-4207-866d-777c140516da', '8n7wn', '2021-11-11 21:12:49');
INSERT INTO `sys_captcha` VALUES ('30605b81-c685-4813-884c-221bee525c73', 'bbafw', '2021-11-11 21:10:53');
INSERT INTO `sys_captcha` VALUES ('38a9cf7c-1738-4ebb-8a81-faf1e9457742', '2aaem', '2021-06-18 10:25:14');
INSERT INTO `sys_captcha` VALUES ('3e244119-5464-4a16-8ec0-1409960cabb5', '2g33y', '2021-11-11 09:14:17');
INSERT INTO `sys_captcha` VALUES ('3ffcd216-37a7-4280-8c15-997528be3656', '646yy', '2021-11-11 21:19:17');
INSERT INTO `sys_captcha` VALUES ('42b0e723-6c71-4303-8b79-4006d5a11f36', 'pya32', '2021-06-18 14:14:06');
INSERT INTO `sys_captcha` VALUES ('4341ed8e-3cf1-41ae-8550-0f6fdd550db3', 'y63pb', '2021-11-11 16:38:23');
INSERT INTO `sys_captcha` VALUES ('43628c5e-fde8-4a1e-864f-c4851e1f6623', 'mpyd8', '2021-11-11 21:08:58');
INSERT INTO `sys_captcha` VALUES ('456cc4b1-eb07-4b27-8b60-c100dcc4a46b', 'c5fd4', '2021-06-18 14:11:19');
INSERT INTO `sys_captcha` VALUES ('4726c8e9-96c2-45e1-8147-b9b4082c4e47', 'xx2n4', '2021-11-11 14:30:24');
INSERT INTO `sys_captcha` VALUES ('4753f337-efb4-4409-8e40-c0548a6eeb40', 'fn6cf', '2021-06-18 14:13:12');
INSERT INTO `sys_captcha` VALUES ('4f7b6f2d-c363-4eb7-8d80-28febc809289', 'e7xfe', '2021-11-11 21:08:46');
INSERT INTO `sys_captcha` VALUES ('5aa132e5-28a1-4fea-8701-8746e8b3889a', '7m6gn', '2021-11-11 16:38:23');
INSERT INTO `sys_captcha` VALUES ('5be713d4-54b7-4dbc-8dab-6df3dfe488be', 'ce6x5', '2021-11-11 09:10:28');
INSERT INTO `sys_captcha` VALUES ('5d0fa942-1e9e-42a9-8a54-12b552c9b8b0', 'cafc4', '2021-06-21 10:21:45');
INSERT INTO `sys_captcha` VALUES ('63c07e18-35b0-4ca7-87d0-c7d65028cfb9', 'wpcpx', '2021-06-18 14:14:00');
INSERT INTO `sys_captcha` VALUES ('64d5994c-ddca-4078-8919-06c7210e3603', 'aep5p', '2021-11-11 21:10:49');
INSERT INTO `sys_captcha` VALUES ('64e263d5-6c46-4d43-8823-33f38e5ab8a2', 'pmebp', '2021-11-11 21:14:06');
INSERT INTO `sys_captcha` VALUES ('65bfb867-ad54-42e4-850d-63e47e5246a4', 'nd3bm', '2021-11-11 21:07:49');
INSERT INTO `sys_captcha` VALUES ('6954fc4e-4eee-4337-818f-d9e630b528b2', '3f4w5', '2021-11-11 21:13:10');
INSERT INTO `sys_captcha` VALUES ('6f081ab6-ce75-4218-8091-4d6edfaf64fa', '4658p', '2021-11-11 21:10:41');
INSERT INTO `sys_captcha` VALUES ('718418ca-e9ee-4dcb-89db-1f8784be0e10', 'dydw4', '2021-11-11 21:09:32');
INSERT INTO `sys_captcha` VALUES ('7196d250-55c0-41df-8921-f4813fcdf875', 'wagx5', '2021-11-11 21:09:23');
INSERT INTO `sys_captcha` VALUES ('737b5772-9e26-409c-8983-42f2b9245f72', 'p7gn8', '2021-06-18 14:08:07');
INSERT INTO `sys_captcha` VALUES ('75f22c1d-2bbf-4512-8a59-9a569673dc81', '8gxm8', '2021-11-11 21:11:18');
INSERT INTO `sys_captcha` VALUES ('77c9b822-b6ce-40e2-866a-2ea69baa5ef5', 'wwg86', '2021-11-11 21:13:04');
INSERT INTO `sys_captcha` VALUES ('813c5291-f7cc-4320-8010-39635c011eab', '72nmx', '2021-11-09 16:30:56');
INSERT INTO `sys_captcha` VALUES ('8356e149-e5d4-4e0a-8ae2-228c66f11b11', 'gaxce', '2021-11-11 21:17:29');
INSERT INTO `sys_captcha` VALUES ('8a332934-9e67-4cae-8a56-2ea4d307d7c0', 'm8en5', '2021-11-11 20:57:30');
INSERT INTO `sys_captcha` VALUES ('940a1c9d-b656-4e39-8ae8-b420ede0f2fe', 'fbdac', '2021-11-11 20:57:19');
INSERT INTO `sys_captcha` VALUES ('972e20c5-6ac9-4d55-8d23-d3343332b231', '7fmf7', '2021-11-11 21:07:33');
INSERT INTO `sys_captcha` VALUES ('981a5219-c1ac-43dd-8f54-f4c9067b352c', 'eed62', '2021-11-11 20:57:23');
INSERT INTO `sys_captcha` VALUES ('9c2c4ecd-b19c-4b4a-825e-7977e074aff6', 'pa5c5', '2021-11-11 14:31:08');
INSERT INTO `sys_captcha` VALUES ('a2479cc1-da72-429a-8b6c-c7bf04b08e49', '58264', '2021-11-11 21:10:04');
INSERT INTO `sys_captcha` VALUES ('a655d796-24c7-47ae-8b84-deb3373101e6', '4g6dn', '2021-11-11 21:10:10');
INSERT INTO `sys_captcha` VALUES ('a8bb1b1a-5445-4204-8b08-c591bbe5d092', 'c7p3p', '2021-11-13 14:17:47');
INSERT INTO `sys_captcha` VALUES ('a93bcf85-2673-4cb4-8f6c-9a037a89fadd', 'da5ne', '2021-11-11 10:21:25');
INSERT INTO `sys_captcha` VALUES ('ab569773-d170-4f99-8f7b-18419bc2b279', 'mbgc3', '2021-11-11 21:08:53');
INSERT INTO `sys_captcha` VALUES ('ac1a5147-e5b8-41d1-8e39-9013e5aa12f8', '47357', '2021-11-11 21:11:59');
INSERT INTO `sys_captcha` VALUES ('ae763031-f726-4177-85c6-ad52e9e72e16', '3m7m3', '2021-11-11 21:08:33');
INSERT INTO `sys_captcha` VALUES ('b5d50a94-9338-45e5-8c91-6e75c14287a1', 'neabn', '2021-11-09 16:28:20');
INSERT INTO `sys_captcha` VALUES ('b8566b0a-3e1d-4d07-8193-b10a222099c2', 'fnfx4', '2021-06-18 15:07:48');
INSERT INTO `sys_captcha` VALUES ('ba1150ca-e1e8-4b1e-84e6-16c305292809', '7nmdp', '2021-11-11 21:16:27');
INSERT INTO `sys_captcha` VALUES ('bbbb65c3-52c9-48a3-83a7-05ac95e24373', 'n8afp', '2021-06-18 15:01:31');
INSERT INTO `sys_captcha` VALUES ('bceb705f-1658-4cbf-812c-929d55b2907b', '887ny', '2021-11-11 21:07:56');
INSERT INTO `sys_captcha` VALUES ('be653cff-842f-4e38-8694-bb51b86f6253', 'mpca6', '2021-11-11 20:48:19');
INSERT INTO `sys_captcha` VALUES ('c04d793f-7a3b-4069-80e3-fec7cbbcbb3f', 'mab3b', '2021-06-18 15:01:11');
INSERT INTO `sys_captcha` VALUES ('c2c7909e-6821-4469-83fd-17aab02c024b', '8c456', '2021-11-09 19:03:43');
INSERT INTO `sys_captcha` VALUES ('caac57ba-8958-436f-8b25-3e0362feda87', 'bnndd', '2021-06-18 14:08:26');
INSERT INTO `sys_captcha` VALUES ('ceb3355c-f878-454a-89db-2a2f1c894f67', 'p3xan', '2021-11-11 21:10:26');
INSERT INTO `sys_captcha` VALUES ('d051072b-4307-4600-8413-54ea0b47528d', 'da3bw', '2021-06-18 15:08:01');
INSERT INTO `sys_captcha` VALUES ('d27dcea8-98ff-48d5-866f-9cc587a72092', 'nbp6b', '2021-11-11 21:17:32');
INSERT INTO `sys_captcha` VALUES ('dbecc500-0ce9-4675-8951-15353aa83de9', '5fn67', '2021-11-11 20:57:12');
INSERT INTO `sys_captcha` VALUES ('ddb5ce74-a8fe-4fae-8214-362b7ceef196', 'ey5y7', '2021-11-11 20:56:54');
INSERT INTO `sys_captcha` VALUES ('dfd6b954-f4ee-4736-86c5-d62d6c56fc03', 'ewfwd', '2021-11-11 21:14:19');
INSERT INTO `sys_captcha` VALUES ('dfd9a830-f24e-4d53-818c-47b445bac95f', 'a756x', '2021-11-09 19:04:14');
INSERT INTO `sys_captcha` VALUES ('e5885796-7d84-4b75-8fc8-8e6d78ddf082', 'w4wy6', '2021-11-11 21:07:35');
INSERT INTO `sys_captcha` VALUES ('f03300c8-8db3-4401-89eb-49d6b6b63c39', 'gepyf', '2021-11-11 21:16:20');
INSERT INTO `sys_captcha` VALUES ('f7955a02-4d33-488b-8feb-5fa1a3600a0b', 'exgx7', '2021-11-11 21:10:33');
INSERT INTO `sys_captcha` VALUES ('f8d2c19d-e956-412f-8843-d3bcebb55a17', '6n6m8', '2021-11-11 21:12:13');
INSERT INTO `sys_captcha` VALUES ('fa3834bb-a9b5-4435-8f66-2fdf20aa77a2', 'gxc8w', '2021-06-18 14:58:37');
INSERT INTO `sys_captcha` VALUES ('faa64fa7-7ddd-4aa7-8fc6-c52cbb7cfcdd', 'f4b24', '2021-11-11 21:12:27');
INSERT INTO `sys_captcha` VALUES ('fc579665-086e-4be7-8459-03ca7746f55d', 'g5gee', '2021-11-11 21:08:12');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `perms` varchar(500) DEFAULT NULL COMMENT 'Permissions',
  `type` int(11) DEFAULT NULL COMMENT 'Type 0: Catalog 1: Menu 2: Button',
  `icon` varchar(50) DEFAULT NULL COMMENT 'Menu Icons',
  `order_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', 'System management', null, null, '0', 'system', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', 'User management', 'sys/user', null, '1', 'admin', '1');
INSERT INTO `sys_menu` VALUES ('3', '1', 'Role management', 'sys/role', null, '1', 'role', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', 'Menu management', 'sys/menu', null, '1', 'menu', '3');
INSERT INTO `sys_menu` VALUES ('5', '2', 'Query', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('6', '2', 'Add', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('7', '2', 'Modify', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('8', '2', 'Delete', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('9', '3', 'Query', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '3', 'Add', null, 'sys:role:save,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '3', 'Modify', null, 'sys:role:update,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '3', 'Delete', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('13', '4', 'Query', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '4', 'Add', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('15', '4', 'Modify', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '4', 'Delete', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('31', '0', 'Book management', '', '', '0', 'menu', '1');
INSERT INTO `sys_menu` VALUES ('32', '31', 'Book management', 'book/book', '', '1', 'zhedie', '0');
INSERT INTO `sys_menu` VALUES ('33', '32', 'Query', null, 'book:book:list,book:book:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('34', '32', 'Add', null, 'book:book:save', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('35', '32', 'Modify', null, 'book:book:update', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('36', '32', 'Delete', null, 'book:book:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('37', '31', 'Type management', 'book/type', null, '1', 'zonghe', '1');
INSERT INTO `sys_menu` VALUES ('38', '37', 'Query', null, 'book:type:list,book:type:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('39', '37', 'Add', null, 'book:type:save,book:type:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('40', '37', 'Modify', null, 'book:type:update,book:type:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('41', '37', 'Delete', null, 'book:type:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('42', '31', 'Book borrowing', 'book/borrow', null, '1', 'sousuo', '2');
INSERT INTO `sys_menu` VALUES ('43', '42', 'Query', null, 'book:book:list,book:book:info,book:type:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('44', '42', 'Borrow', null, 'book:book:borrow', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('45', '48', 'Reserve\n', '', 'book:book:reserve', '1', '', '3');
INSERT INTO `sys_menu` VALUES ('48', '31', 'My Borrow', 'book/myBorrow', null, '1', 'geren', '4');
INSERT INTO `sys_menu` VALUES ('49', '48', 'Query', null, 'book:borrow:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('50', '31', 'Borrow Overdue', 'book/borrow-overdue', 'book:borrow:list', '1', 'geren', '5');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'Student', 'Book borrowing、My Borrow', '1', '2021-06-21 17:08:11');
INSERT INTO `sys_role` VALUES ('2', 'Admin', '', '1', '2021-11-09 17:27:52');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `menu_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('77', '2', '32');
INSERT INTO `sys_role_menu` VALUES ('78', '2', '33');
INSERT INTO `sys_role_menu` VALUES ('79', '2', '34');
INSERT INTO `sys_role_menu` VALUES ('80', '2', '35');
INSERT INTO `sys_role_menu` VALUES ('81', '2', '36');
INSERT INTO `sys_role_menu` VALUES ('82', '2', '37');
INSERT INTO `sys_role_menu` VALUES ('83', '2', '38');
INSERT INTO `sys_role_menu` VALUES ('84', '2', '39');
INSERT INTO `sys_role_menu` VALUES ('85', '2', '40');
INSERT INTO `sys_role_menu` VALUES ('86', '2', '41');
INSERT INTO `sys_role_menu` VALUES ('87', '2', '50');
INSERT INTO `sys_role_menu` VALUES ('88', '2', '-666666');
INSERT INTO `sys_role_menu` VALUES ('89', '2', '31');
INSERT INTO `sys_role_menu` VALUES ('90', '1', '42');
INSERT INTO `sys_role_menu` VALUES ('91', '1', '43');
INSERT INTO `sys_role_menu` VALUES ('92', '1', '44');
INSERT INTO `sys_role_menu` VALUES ('93', '1', '48');
INSERT INTO `sys_role_menu` VALUES ('94', '1', '45');
INSERT INTO `sys_role_menu` VALUES ('95', '1', '49');
INSERT INTO `sys_role_menu` VALUES ('96', '1', '-666666');
INSERT INTO `sys_role_menu` VALUES ('97', '1', '31');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `salt` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT 'Status 0: Disabled 1: Normal',
  `create_user_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'cdac762d0ba79875489f6a8b430fa8b5dfe0cdd81da38b80f02f33328af7fd4a', 'YzcmCZNvbXocrsz9dm8e', '79134582@qq.com', '12346678910', '1', '1', '2016-11-11 11:11:11');
INSERT INTO `sys_user` VALUES ('2', 'Student1', '6a560132a45ba95257814c6b021557c9e9acb7da77d0a65ac134ffce744b03bb', 'l2I5PlmpSwT32vr0woZU', '123456@qq.com', '18155907070', '1', '1', '2021-06-21 17:09:21');
INSERT INTO `sys_user` VALUES ('3', 'Admin1', '5077e487cc26d7f999612fea29875423e73eed4f9d0c32a9d053fb08bf2af657', 'J0Z3nGkcLpvDMIIOY32N', '1111@qq.com', '13333333333', '1', '1', '2021-11-09 17:28:20');
INSERT INTO `sys_user` VALUES ('4', 'Student2', '42aa65ff87ef2bf684fca381f29044495128af7f77f108523dcc229fbf065155', '0REnEbAShf0ThaDkSaYR', '11111@qq.com', '13333333333', '1', '1', '2021-11-11 15:06:48');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('2', '2', '1');
INSERT INTO `sys_user_role` VALUES ('3', '3', '2');
INSERT INTO `sys_user_role` VALUES ('4', '4', '1');

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES ('1', 'd4a1a8ff529e23d24b4f605c763b3eb9', '2021-11-13 04:46:06', '2021-11-12 16:46:06');
INSERT INTO `sys_user_token` VALUES ('2', 'd1d464a1f486cc9f54cee1b4a473a4fd', '2021-11-15 03:46:51', '2021-11-14 15:46:51');
INSERT INTO `sys_user_token` VALUES ('3', 'f0478d99b8f52111fc338a5065577a1f', '2021-11-12 09:14:23', '2021-11-11 21:14:23');
INSERT INTO `sys_user_token` VALUES ('4', 'ea7f859a3693e0468bbab0347119c574', '2021-11-12 03:45:31', '2021-11-11 15:45:31');
